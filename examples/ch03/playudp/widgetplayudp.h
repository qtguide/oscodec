#ifndef WIDGETPLAYUDP_H
#define WIDGETPLAYUDP_H

#include <QWidget>
#include <QAudioDeviceInfo> //音频设备
#include <QAudioFormat> //音频格式
#include <QAudioOutput> //音频输出
#include <QFile>    //保存文件
#include <QUdpSocket>   //UDP套接字
#include <QNetworkInterface>    //网卡接口信息，查询本地IP
#include <QHostAddress> //IP地址

namespace Ui {
class WidgetPlayUDP;
}

class WidgetPlayUDP : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetPlayUDP(QWidget *parent = 0);
    ~WidgetPlayUDP();

private slots:
    void on_comboBoxDeviceOut_currentIndexChanged(int index);

    void on_pushButtonStart_clicked();

    void on_pushButtonStop_clicked();
    //输出对象状态变化
    void onStateChanged(QAudio::State state);
    //接收UDP数据
    void recvUDPData();

private:
    Ui::WidgetPlayUDP *ui;
    //音频设备列表
    QList<QAudioDeviceInfo> m_listDevices;
    //音频格式
    QAudioFormat m_aFormat;
    //音频输出对象指针
    QAudioOutput *m_pAudioOut;
    //内存缓冲区设备指针
    QIODevice *m_pBuffDevice;

    //保存文件
    QFile m_fileSave;

    //初始化，获取音频输出设备列表
    void InitFindDevices();
    //保存采样点类型的字符串列表
    QList<QString> m_listSampleTypes;

    //开始播放时，禁止修改参数，停止后恢复
    void holdParras(bool bHold);

    //网络套接字的指针
    QUdpSocket *m_pSocket;
};

#endif // WIDGETPLAYUDP_H
