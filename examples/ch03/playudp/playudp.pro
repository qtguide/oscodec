#-------------------------------------------------
#
# Project created by QtCreator 2018-10-30T07:58:57
#
#-------------------------------------------------

QT       += core gui multimedia network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = playudp
TEMPLATE = app


SOURCES += main.cpp\
        widgetplayudp.cpp

HEADERS  += widgetplayudp.h

FORMS    += widgetplayudp.ui
