#ifndef WIDGETARECORDER_H
#define WIDGETARECORDER_H

#include <QWidget>
#include <QAudioRecorder>//录音
#include <QAudioEncoderSettings>//音频编码器
#include <QAudioBuffer>//音频缓存
#include <QAudioProbe> //音频探针
#include <QUdpSocket> //UDP套接字
#include <QHostAddress> //主机地址
#include <QUrl> //文件地址

namespace Ui {
class WidgetARecorder;
}

class WidgetARecorder : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetARecorder(QWidget *parent = 0);
    ~WidgetARecorder();

private slots:
    void on_pushButtonStart_clicked();

    void on_pushButtonStop_clicked();

    //手动添加槽函数
    //用户异步状态监测，反映录音对象的实际工作状态
    void onStatusChanged(QMediaRecorder::Status status);
    //出错信息监测
    void onError(QMediaRecorder::Error error);
    //录音时长监测，单位毫秒
    void onDurationChanged(qint64 duration);

    //实时获取音频数据并发送到网络
    void onAudioBufferProbed(const QAudioBuffer & buffer);

private:
    Ui::WidgetARecorder *ui;
    //录音对象
    QAudioRecorder m_aRecorder;
    //编码器对象
    QAudioEncoderSettings m_aEncoder;
    //音频探针对象
    QAudioProbe m_aProbe;
    //套接字指针
    QUdpSocket *m_pSocket;

    //界面初始化函数
    void InitUI();
    //录音时禁止修改参数
    void holdParas(bool bHold);
};

#endif // WIDGETARECORDER_H
