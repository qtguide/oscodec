#-------------------------------------------------
#
# Project created by QtCreator 2018-11-19T10:00:33
#
#-------------------------------------------------

QT       += core gui multimedia network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = arecorder
TEMPLATE = app


SOURCES += main.cpp\
        widgetarecorder.cpp

HEADERS  += widgetarecorder.h

FORMS    += widgetarecorder.ui
