#ifndef WIDGETPLAYFILE_H
#define WIDGETPLAYFILE_H

#include <QWidget>
#include <QAudioDeviceInfo>//音频设备信息
#include <QAudioFormat> //音频格式
#include <QAudioOutput> //音频输出类
#include <QFile>    //文件

namespace Ui {
class WidgetPlayFile;
}

class WidgetPlayFile : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetPlayFile(QWidget *parent = 0);
    ~WidgetPlayFile();

private slots:
    void on_comboBoxDeviceOut_currentIndexChanged(int index);

    void on_pushButtonStart_clicked();

    void on_pushButtonPause_clicked();

    void on_pushButtonStop_clicked();
    //输出对象状态变化
    void onStateChanged(QAudio::State state);
    //定时通知
    void onNotify();

private:
    Ui::WidgetPlayFile *ui;
    //设备列表
    QList<QAudioDeviceInfo> m_listDevices;
    //音频格式
    QAudioFormat m_aFormat;
    //音频输出对象指针
    QAudioOutput *m_pOutput;

    //播放文件
    QFile m_filePlay;

    //初始化，获取音频输出设备列表
    void InitFindDevices();
    //保存采样点类型的字符串列表
    QList<QString> m_listSampleTypes;

    //开始播放时，禁止修改音频参数，停止后恢复
    void holdParas(bool bHold);

};

#endif // WIDGETPLAYFILE_H
