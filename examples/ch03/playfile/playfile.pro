#-------------------------------------------------
#
# Project created by QtCreator 2018-10-26T15:35:19
#
#-------------------------------------------------

QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = playfile
TEMPLATE = app


SOURCES += main.cpp\
        widgetplayfile.cpp

HEADERS  += widgetplayfile.h

FORMS    += widgetplayfile.ui
