#ifndef WIDGETPAINCOMPLEX_H
#define WIDGETPAINCOMPLEX_H

#include <QWidget>
#include <portaudio.h> //PA库
#include <QFile>//文件
#include <QUdpSocket>//UDP套接字
#include <QHostAddress>//IP地址类
#include <QElapsedTimer>//计时器类

namespace Ui {
class WidgetPAInComplex;
}

//自定义回调函数声明
static int audioInCallback(const void *inputBuffer,
                       void *outputBuffer,
                       unsigned long frameCount,
                       const PaStreamCallbackTimeInfo* timeInfo,
                       PaStreamCallbackFlags statusFlags,
                       void *userData ) ;


class WidgetPAInComplex : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetPAInComplex(QWidget *parent = 0);
    ~WidgetPAInComplex();
    //声明公开变量，方便回调函数访问
    //音频流
    PaStream *m_streamIn;
    //输入声道数
    int m_nChannels;
    //采样位宽，比特数
    int m_nSampleSize;
    //采样频率
    int m_nSampleRate;
    //时间片内帧数
    int m_nFramesPerBuffer;
    //套接字
    QUdpSocket m_socket;
    //文件
    QFile m_file;
    //计时器
    QElapsedTimer m_calcTime;
    //状态显示函数
    void showStatus(int nFrames, int nBytes);
    //根据采样位宽获取采样点数据格式
    PaSampleFormat getSampleFormat(int sampleSize);

private slots:
    void on_comboBoxHostAPI_currentIndexChanged(int index);

    void on_pushButtonStart_clicked();

    void on_pushButtonStop_clicked();

private:
    Ui::WidgetPAInComplex *ui;
    //界面初始化
    void initUI();
    //录音时固定参数
    void holdParas( bool bHold );
};

#endif // WIDGETPAINCOMPLEX_H
