#-------------------------------------------------
#
# Project created by QtCreator 2018-11-27T18:38:43
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = paincomplex
TEMPLATE = app


SOURCES += main.cpp\
        widgetpaincomplex.cpp

HEADERS  += widgetpaincomplex.h

FORMS    += widgetpaincomplex.ui

LIBS += -lportaudio
