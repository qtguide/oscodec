#-------------------------------------------------
#
# Project created by QtCreator 2018-10-23T08:30:58
#
#-------------------------------------------------

QT       += core gui multimedia network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = audioin
TEMPLATE = app


SOURCES += main.cpp\
        widgetaudioin.cpp

HEADERS  += widgetaudioin.h

FORMS    += widgetaudioin.ui
