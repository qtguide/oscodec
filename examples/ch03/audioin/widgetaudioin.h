#ifndef WIDGETAUDIOIN_H
#define WIDGETAUDIOIN_H

#include <QWidget>
#include <QAudioDeviceInfo> //声卡设备信息
#include <QAudioFormat>     //音频采样格式
#include <QAudioInput>      //音频输入
#include <QFile>        //保存文件
#include <QUdpSocket>   //发送UDP

namespace Ui {
class WidgetAudioIn;
}

class WidgetAudioIn : public QWidget
{
    Q_OBJECT

public:
    explicit WidgetAudioIn(QWidget *parent = 0);
    ~WidgetAudioIn();

private slots:
    void on_comboBoxDeviceIn_currentIndexChanged(int index);

    void on_pushButtonStart_clicked();

    void on_pushButtonStop_clicked();

    //音频输入设备状态变化监控
    void onDeviceStateChanged(QAudio::State state);
    //缓冲区数据可读
    void onBuffReadyRead();


private:
    Ui::WidgetAudioIn *ui;
    //保存音频输入对象的指针
    QAudioInput *m_pInput;
    //保存内存缓冲区设备指针
    QIODevice *m_pBuffDevice;
    //音频采样格式
    QAudioFormat m_aFormat;
    //音频设备列表
    QList<QAudioDeviceInfo> m_listDevices;

    //保存文件对象
    QFile m_fileSave;
    //发送网络套接字指针
    QUdpSocket *m_pSocket;

    //初始化，获取音频设备
    void InitFindDevices();
    //保存采样点类型的字符串列表
    QList<QString> m_listSampleTypes;

    //开始采样时，禁止修改采样参数，停止后恢复
    void holdParas(bool bHold);
};

#endif // WIDGETAUDIOIN_H
