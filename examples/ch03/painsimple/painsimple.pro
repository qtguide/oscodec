#-------------------------------------------------
#
# Project created by QtCreator 2018-11-24T10:06:01
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = painsimple
TEMPLATE = app


SOURCES += main.cpp\
        widgetpainsimple.cpp

HEADERS  += widgetpainsimple.h

FORMS    += widgetpainsimple.ui

LIBS += -lportaudio
