# oscodec

#### 项目介绍  
本教程用于介绍开源音频、视频编解码库和封装格式，目前先从  Xiph.Org Foundation  的开源软件写起。
软件库的官方页面：  
https://xiph.org/  
代码示例采用Qt 5编写，教程按照 GNU Free Documentation License 发布。  
Qt开发参考教程：  
https://qtguide.ustclug.org/  
本教程预览页面：  
https://qtguide.gitee.io/oscodec  

#### 目录说明  
所有的示例代码放在 examples 目录；  
所有的开发库放在 libs 目录。  

